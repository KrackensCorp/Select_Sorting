#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
 
double wtime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec/1E6;
}

int main()
{
    int N;
    printf("N: ");
    scanf("%d", &N);
    int* mass;
    mass = (int *)malloc(N * sizeof(int));
    printf("Elements: ");
    for (int i = 0; i < N; i++)
        scanf("%d", &mass[i]);
    int tmp;
    char noSwap;
    
    double t = wtime();
    
    for (int i = N - 1; i >= 0; i--)
    {
        noSwap = 1;
        for (int j = 0; j < i; j++)
        {
            if (mass[j] > mass[j + 1])
            {
                tmp = mass[j];
                mass[j] = mass[j + 1];
                mass[j + 1] = tmp;
                noSwap = 0;
            }
        }
        if (noSwap == 1)
            break;
    }
    printf("Вывод:\n");
    for (int i = 0; i < N; i++)
        printf("%d ", mass[i]);
    printf("\n");
            t = wtime() - t;
    printf("%lf", t);
    printf("\n");
    free(mass);
    return 0;
}
/* построить граффики с gnuplot, сделать 3 сортировки с сайта по вариантам */