#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int findLargestNum(int* arr, int size){

  int i;
  int largestNum = -1;

  for(i = 0; i < size; i++){
    if(arr[i] > largestNum)
      largestNum = arr[i];
  }

  return largestNum;
}

void radix_sort(int * arr, int size){

  int i;
  int semiSorted[size];
  int significantDigit = 1;
  int largestNum = findLargestNum(arr, size);

  while (largestNum / significantDigit > 0){
    int bucket[10] = { 0 };

    for (i = 0; i < size; i++)
      bucket[(arr[i] / significantDigit) % 10]++;

    for (i = 1; i < 10; i++)
      bucket[i] += bucket[i - 1];

    for (i = size - 1; i >= 0; i--)
      semiSorted[--bucket[(arr[i] / significantDigit) % 10]] = arr[i];

    for (i = 0; i < size; i++)
      arr[i] = semiSorted[i];

    significantDigit *= 10;

  }
}


int main()
{
    srand(time(NULL));
    int i, size;
    printf ("Введите количетсво элементов: ");
    scanf ("%d", &size);
    int arr[size];
    for (i = 0; i < size; i++) {
        arr[i] = rand()%1000;
    }
    double start = clock ();
    radix_sort (arr, size);
    double end = clock ();
    double time = difftime(end, start) / 1000000;
    printf ("Время выполнения: %f \n", time);
    return 0;
}

