#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void timeH()
{    
   long int s_time;
   struct tm *m_time;
   char str_t[128]="";

   s_time = time (NULL);
   m_time = localtime (&s_time);

   strftime (str_t, 128, "%x %X", m_time);
   printf ("%20s\n",str_t);
}

void countSort(int lon)
{   
    int j, stime;
    int arr[lon];
    long ltime;
    ltime = time (NULL);
    stime = (unsigned int) ltime/2;
    srand(stime);
    for (j = 0; j < lon; j++) {
        arr[j] = rand() % lon;
    }
    int max = arr[0];
    double start = clock();

    for (int i = 1; i < lon; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    int mem[max];
    memset(mem, 0, sizeof(mem));
    for (j = 0; j < lon; j++) {
        for (int i = 0; i < max; i++) {
            if (arr[j] == i) {
                mem[i]+= 1;
            }
        }
    }
    memset(arr, 0 , sizeof(arr));
    int g = 0;
    for (int i = 0; i < max; i++) {
        if (mem[i] != 0) {
            for (int f = 0; g + f <= g + mem[i]; f++) {
                arr[g + f] = i;
            }
            g = g + mem[i];
        }
    }
    printf("%f c", (clock() - start) / CLOCKS_PER_SEC);
    timeH();
    memset(mem, 0, sizeof(mem));
    memset(arr, 0, sizeof(arr));
}

void BubbleSort(int lon)
{
    int j, stime;
    int arr[lon];
    long ltime;
    ltime = time (NULL);
    stime = (unsigned int) ltime/2;
    srand(stime);
    for (j = 0; j < lon; j++) {
        arr[j] = rand() % lon;
    }
    double start = clock();

    int s = 1, tmp;
    while (s != 0) {
        s = 0;
        for (int i = 0; i < lon; i++) {
            if (arr[i] > arr[i + 1]) {
                tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                s = 1;
            }
        }
    }

    printf("%f c", (clock() - start) / CLOCKS_PER_SEC);
    timeH();
    memset(arr, 0, sizeof(arr));
}

void InsertionSort(int lon)
{
    int j, stime;
    int arr[lon];
    long ltime;
    ltime = time (NULL);
    stime = (unsigned int) ltime/2;
    srand(stime);
    for (j = 0; j < lon; j++) {
        arr[j] = rand() % lon;
    }
    double start = clock();
    int temp;
		
	for (int i = 0; i < lon; i++) {
		j = i;
		while (j > 0 && arr[j] < arr[j - 1]) {
			temp = arr[j];
			arr[j] = arr[j - 1];
			arr[j - 1] = temp;
			j--;
		}
	}

    printf("%f c", (clock() - start) / CLOCKS_PER_SEC);
    timeH();
    memset(arr, 0, sizeof(arr));
    
}

void RadixSort(int lon) 
{
    int stime;
    int j;
    int n = 0;
    int exp = 1;
    int m = 0;
    int arr[lon];
    long ltime;
    ltime = time (NULL);
    stime = (unsigned int) ltime/2;
    srand(stime);
    for (j = 0; j < lon; j++) {
        arr[j] = rand() % lon;
    }
    double start = clock();
    
    for (j = 0; j < n;  j++) {
      if (arr[j] > m)
      m = arr[j];}
    
    while (m / exp >  0) {
      int box[10] = { 0 };
      
      for (j = 0; j <  n; j++)
        box[arr[j] / exp %  10]++;
      for (j = 1; j <  10; j++)
        box[j] += box[j -  1];
      for (j = n - 1; j  >= 0; j--)
        arr[--box[arr[j] / exp  % 10]] = arr[j];
      for (j = 0; j <  n; j++)
        arr[j] = arr[j];
    exp *= 10;
    }

    printf("%f c", (clock() - start) / CLOCKS_PER_SEC);
    timeH();
    memset(arr, 0, sizeof(arr));
}

void SelectionSort(int lon)
{

}

void HeapSort(int lon)
{

}


int main()
{
    printf(" Выберите сортировку: \n1. Count Sort \n2. Bubble Sort \n3. Insertion Sort \n4. Radix Sort \n5. Selection Sort (в разработке) \n6. Heap Sort (в разработке) \nВыбор: ");
    int ask, i, d, k;
    scanf ("%d", &ask);
    printf("Начальная точка: ");
    scanf("%d", &i);
    printf("Шаг: ");
    scanf("%d", &d);
    printf("Конечная точка: ");
    scanf("%d", &k);

    if (ask == 1) {
        printf("----------Count Sort---------\n");
        for (; i <= k; i+= d) {
            printf("long: %3d ----> time: ", i);
            countSort(i);
        }
    } 
    if (ask == 2) {
        printf("----------Bubble Sort---------\n");
        for (; i <= k; i+= d) {
            printf("long: %3d ----> time: ", i);
            BubbleSort(i);
        }
    } 
    if (ask == 3) {
        printf("----------Insertion Sort---------\n");
        for (; i <= k; i+= d) {
            printf("long: %3d ----> time: ", i);
            InsertionSort(i);
        }
    } 
    if (ask == 4) {
        printf("----------RadixSort---------\n");
        for (; i <= k; i+= d) {
            printf("long: %3d ----> time: ", i);
            RadixSort(i);
        }
    } 
    if (ask == 5) {
        printf("----------SelectionSort---------\n");
        for (; i <= k; i+= d) {
            printf("long: %3d ----> time: ", i);
            SelectionSort(i);
        }
    } 
    if (ask == 6) {
        printf("----------HeapSort---------\n");
        for (; i <= k; i+= d) {
            printf("long: %3d ----> time: ", i);
            HeapSort(i);
        }
    }
    
    printf("Done.\n");
    return 0;
}