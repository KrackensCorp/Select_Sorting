#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void heapify(int *first, int leng, int i)
{
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < leng && first[l] > first[largest]){
        largest = l;
    }

    if (r < leng && first[r] > first[largest]){
        largest = r;
    }

    if (largest != i){
        swap(&first[largest], &first[i]);
        heapify(first, leng, largest);
    }
}

void heapsort(int *first, int leng){
    for (int i = leng / 2 - 1; i >= 0; i--){
        heapify(first, leng, i);
    }
    for (int i = leng - 1; i >= 0; i--){
        swap(&first[0], &first[i]);
        heapify(first, i, 0);
    }
}

int main()
{
    int leng = 0;
    printf ("Введите количество элемнтов: ");
    scanf("%d", &leng);
    int first[leng];
    srand(time(NULL)); 
    for (int i = 0; i < leng; i++) {
        first[i] = rand()%100000; 
    }
    double start = clock();
    heapsort(first, leng);
    double end = clock();
    double time = difftime(end, start) / 1000000;
    printf("\n");
    printf("Время выполнения сортировки: %f \n", time);
}
